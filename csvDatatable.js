import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import readCSV from '@salesforce/apex/CSVDatatableController.readCSVFile';
 
const columns = [
    { label: 'Name', fieldName: 'Name', type: 'text' },
    { label: 'Phone', fieldName: 'Phone', type: 'phone' },
    { label: 'Website', fieldName: 'Website', type: 'url' },
    { label: 'Rating', fieldName: 'Rating', type: 'text' }
];
 
export default class CsvDatatable extends LightningElement {
    @api recordId;
    @track error;
    @track columns = columns;
    @track data;
 
    // accepted parameters
    get acceptedFormats() {
        return ['.csv'];
    }
 
    handleUploadFinished(event) {
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
 
        // calling apex class
        readCSV({ idContentDocument: uploadedFiles[0].documentId })
            .then(result => {
                this.data = result;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Accounts are created based CSV file!',
                        variant: 'success',
                    }),
                );
            })
            .catch(error => {
                this.error = error;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error',
                        message: JSON.stringify(error),
                        variant: 'error',
                    }),
                );
            })
 
    }
}
