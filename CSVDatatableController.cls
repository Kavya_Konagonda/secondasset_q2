/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 06-17-2021
 * @last modified by  : kavya.konagonda@mtxb2b.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   06-17-2021   kavya.konagonda@mtxb2b.com   Initial Version
**/
public with sharing class CSVDatatableController {
 
    final private static String NO_FILE_EXCESS_EXCEPTION = 'Files are not accessible by the current user.';
    final private static String NO_ACCOUNT_EXCESS_EXCEPTION = 'Either Account object or Account fields are not accessible by the current user.';
 
    /**
     * @param Id This method takes contentDocumentId as a paramater to process data. This Id is passed from Javascript controller.
     * 
     * Based on the file uplaoded, the method iterates over each row and assigns column values to account fields. After the list of
     * Account objects is ready, the method creates the new Account records into the system.
     * 
     * @return List<Account> The method also returns the list of inserted accounts to show these accounts in frontend after insertion.
     */
    @AuraEnabled
    public static List<Account> readCSVFile(Id idContentDocument){
        list<Account> lstAccsToInsert = new list<Account>();
 
        try {
            if(idContentDocument != null) {
                // getting File Data based on document id 
                if(ContentVersion.SObjectType.getDescribe().isAccessible() && 
                    Schema.SObjectType.ContentVersion.fields.VersionData.isAccessible() ) {
 
                    ContentVersion objVersion = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId =:idContentDocument];
                    // split the file data
                    list<String> lstCSVLines = objVersion.VersionData.toString().split('\n');
 
                    for(Integer i = 1; i < lstCSVLines.size(); i++){
                        Account objAcc = new Account();
                        list<String> csvRowData = lstCSVLines[i].split(',');
                        objAcc.Name = csvRowData[0]; // accName
                        objAcc.Phone = csvRowData[1];
                        objAcc.Website = csvRowData[3];
                        objAcc.Rating = csvRowData[4];
                        lstAccsToInsert.add(objAcc);
                    }
 
                    if(!lstAccsToInsert.isEmpty() && Account.SObjectType.getDescribe().isCreateable() &&
                        Schema.SObjectType.Account.fields.Name.isAccessible() && 
                        Schema.SObjectType.Account.fields.Phone.isAccessible() &&
                        Schema.SObjectType.Account.fields.Website.isAccessible() &&
                        Schema.SObjectType.Account.fields.Rating.isAccessible()) {
                            insert lstAccsToInsert;                        
                    } else {
                        throw new CSVDatatableControllerException(NO_ACCOUNT_EXCESS_EXCEPTION);
                    }
                } else {
                    throw new CSVDatatableControllerException(NO_FILE_EXCESS_EXCEPTION);
                }
            }
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        } 
        return lstAccsToInsert;    
    }
 
    //Public class to throw Custom Exception
    public class CSVDatatableControllerException extends Exception {}
}